require.config({
	paths: {
		"jquery" : "vendor/jquery/jquery",
		"angular" : "vendor/angular/angular",
		"angularResource" : "vendor/angular-resource/angular-resource",
        "angularRoute" : "vendor/angular-route/angular-route",
        "lodash": "vendor/lodash/dist/lodash",
        "angularGoogleMap" : "vendor/angular-google-maps/dist/angular-google-maps.min",
        "moment" : "vendor/moment/moment"
	},
	shim: {
    'angular' : { 'exports' : 'angular' },
    'angularResource' : { deps:['angular'] },
    'jquery': { 'exports' : 'jquery' },
    'angularRoute' : { deps:['angular'] },
    'angularGoogleMap' : { deps:['angular', 'jquery'] },
    'moment' : { 'exports' : 'moment'}
}
});

require(['jquery', 'angular','lodash', 'angularGoogleMap', 'moment', 'routes/common',  'routes/grokalRoutes'] , function ($, angular, _, angularGoogleMap, moment, common, grokalRoutes) {
  require(['directives/header','directives/navigation'], function(){
    $(function () {
      angular.bootstrap(document , ['common', 'home', 'business']);
    });
  })
    

});