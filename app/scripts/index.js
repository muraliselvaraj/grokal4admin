window.fbAsyncInit = function() {
  FB.init({ appId: '463152570452931', 
        status: true, 
        cookie: true,
        version: 'v2.0',
        xfbml: true,
        oauth: true});

  function updateButton(response) {
    var button = document.getElementsByClassName('fb-login');
    console.log(response);
    if (response.authResponse) {
      //user is already logged in and connected
      var userInfo = document.getElementById('user-info');
      FB.api('/me', function(me) {
        console.log(me);
        var d = new Date();
        d.setTime(d.getTime() + (12*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = "user"+"="+JSON.stringify(me)+"; "+expires;
        // console.log(response);
        getProfileData(response, me);
      });
    } else {
      //user is not connected to your app or logged out
        $('.fb-login').click(function() {
          FB.login(function(response) {
            if (response.authResponse) {
              FB.api('/me', function(me) {
                var userInfo = document.getElementById('user-info');
                console.log(me);
                var d = new Date();
                d.setTime(d.getTime() + (12*24*60*60*1000));
                var expires = "expires=" + d.toGMTString();
                document.cookie = "user"+"="+JSON.stringify(me)+"; "+expires;
                getProfileData(response, me);
              });    
            } else {
              //user cancelled login or did not grant authorization
            }
          }, {scope:'email'}); 
        });
      // } 
    }
  }

  // run once with current status and whenever the status changes
  FB.getLoginStatus(updateButton);
  FB.Event.subscribe('auth.statusChange', updateButton);    
};
    
function getProfileData(response, me){
  // console.log(email);
  console.log(response);
  // $(location).attr('href',"dashboard.html");
  
  var data = {
    provider: "facebook",
    userId: me.id,
    token: response.authResponse.accessToken,
    email: me.email
  };
  console.log("Response Data===> ",data);
  var api_base = "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/" 
    $.ajax({
      type: "POST",
      url: api_base + "login",
      dataType : 'json',
      contentType : 'application/json',
      data: JSON.stringify(data)
    })
    .done(function( res ) {
        console.log('Response==>',res);
        $(location).attr('href',"dashboard.html");
    });
}

