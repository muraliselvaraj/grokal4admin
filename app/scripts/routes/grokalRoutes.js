define(['modules/home', 'modules/business', 'controllers/homeController', 'controllers/businessController', 
        'controllers/businessDetailController', 'controllers/updateBusinessController', 'controllers/addBusinessController', 
        'controllers/offersController', 'controllers/offersDetailController', 'controllers/addOfferController', 
        'controllers/updateOfferController', 'controllers/addCategoryController', 'controllers/addCityController', 
        'services/businessOffersService'] , function (grokalRoutes) {
  return grokalRoutes.config(['$routeProvider', function ($routeProvider) {
    	$routeProvider
    		.when('/', {
    			  controller : 'homeController' , 
    			  templateUrl : 'templates/home.html'    				
        })
    		.when('/business_list', {
         	  controller : 'businessController' , 
         	  templateUrl : 'templates/business_list.html'            
        })
        .when('/business_list/:bizId', {
        	  controller : 'businessDetailController',
        	  templateUrl : 'templates/business_details.html'
        })
        .when('/update_business/:businessID', {
            controller : 'updateBusinessController',
            templateUrl : 'templates/update_business.html'
        })
        .when('/add_business', {
        	  controller : 'addBusinessController',
        	  templateUrl : 'templates/add_business.html'
        })
        .when('/add_city', {
            controller : 'addCityController',
            templateUrl : 'templates/add_city.html'
        })
        .when('/offers_list', {
        	  controller : 'offersController',
        	  templateUrl : 'templates/offers_list.html'
        })
        .when('/offers_list/:catId', {
	          controller : 'offersDetailController' , 
	          templateUrl : 'templates/offers_details.html'            
        })
        .when('/add_Offer/:businessID', {
         	  controller : 'addOfferController' , 
         	  templateUrl : 'templates/add_offer.html'            
        })
        .when('/update_Offer/:offerId', {
          controller : 'updateOfferController',
          templateUrl : 'templates/update_offer.html'
        })
        .when('/add_category', {
         	  controller : 'addCategoryController' , 
         	  templateUrl : 'templates/add_category.html'            
        })
    }]);
});


