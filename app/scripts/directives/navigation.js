define(['modules/common'], function(app) {
	app.directive('navigation', function() {
		return {
			restrict: 'E',
			templateUrl: 'templates/navigation.html'
		}
	})
})