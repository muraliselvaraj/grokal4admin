define(['modules/common'], function(app) {
	app.directive('header', function() {
		return {
			restrict: 'E',
			templateUrl: 'templates/header.html'
		}
	})
})