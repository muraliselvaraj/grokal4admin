define(['angular', 'angularResource', 'angularRoute', 'angularGoogleMap'] , function (angular) {
  return angular.module('home' , ['ngResource', 'business' , 'common']);
});