define(['angular' , 'angularResource','angularRoute'] , function (angular) {
  return angular.module('common' , ['ngResource','ngRoute']);
});