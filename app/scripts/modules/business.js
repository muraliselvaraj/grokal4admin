define(['angular' , 'angularResource', 'angularRoute', 'angularGoogleMap'] , function (angular) {
  return angular.module('business' , ['ngResource', 'common']);
}); 