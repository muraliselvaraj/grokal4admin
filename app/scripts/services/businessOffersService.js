define(['modules/business'] , function (businessModule) {
    var api_base = "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/";    
  	businessModule.service('businessService', ['$q', '$http', function ($q, $http) {
    	
    	this.businessList = function () {
    		var defer = $q.defer();
    		
    		$http({
    			url: api_base+'getAllBiz',
    			method: 'get'
	    	})
	    	.success(function(data) {
	    		console.log(data);
	    		defer.resolve(data);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };

	    this.selectedBusiness = function(bizId) {
	    	var defer = $q.defer();
	    	$http({
	    		url : api_base+'getBiz?bizId='+bizId,
	    		method : 'get',
	    		dataType : 'json',
	    		contentType : 'application/json'
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };

	    this.updatingBusiness = function(bizsData, bizId) {
	    	var defer = $q.defer();
    		$http({
    			url : api_base+'updateBiz?bizId='+bizId,
    			method : 'POST',
    			dataType : 'json',
    			contentType : 'application/json',
    			data : JSON.stringify(bizsData)
    		})
    		.success(function(res) {
    			console.log(res);
    			defer.resolve(res);
    		})
    		.error(function(err) {
    			console.log(err);
    			defer.reject(err);
    		});
    		return defer.promise;
	    };

	    this.businessOffers = function (bizId) {
    		var defer = $q.defer();
    		$http({
    			url : api_base+'getAccountBizOffers?bizId='+bizId,
    			method : 'get',
    			dataType : 'json',
    			contentType : 'application/json'
    		})
    		.success(function(data) {
    			console.log(data);
    			defer.resolve(data);
    		})
    		.error(function(err) {
    			console.log(err);
    			defer.reject(err);
    		});
    		return defer.promise;
    		
	    };

	    this.newCategory = function (categoryInfo) {
    		var defer = $q.defer();
    		
    		$http({
    			url : api_base+'addCategory',
    			method : 'POST',
    			dataType : 'json',
    			contentType : 'application/json',
    			data : JSON.stringify(categoryInfo)
	    	})
	    	.success(function(data) {
	    		console.log(data);
	    		defer.resolve(data);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(data);
	    	});
	    	return defer.promise;
	    };

	    this.categoryList = function () {
    		var defer = $q.defer();
    		
    		$http({
    			url: api_base+'getAllCategories',
    			method: 'get'
	    	})
	    	.success(function(data) {
	    		console.log(data);
	    		defer.resolve(data);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(data);
	    	});
	    	return defer.promise;
	    };

	    this.selectedCategory = function(catId) {
	    	var defer = $q.defer();

	    	$http({
	    		url : api_base+'getCategory?catId='+catId,
	    		method : 'GET',
	    		dataType : 'json',
	    		contentType : 'application/json'
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});

	    	return defer.promise;
	    };


	    this.getAllCities = function () {
    		var defer = $q.defer();
    		
    		$http({
    			url: api_base+'getAllCities',
    			method: 'get'
	    	})
	    	.success(function(data) {
	    		console.log(data);
	    		defer.resolve(data);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(data);
	    	});
	    	return defer.promise;
	    };

	    this.newCity = function (cityInfo) {
    		var defer = $q.defer();
    		
    		$http({
    			url : api_base+'addCity',
    			method : 'POST',
    			dataType : 'json',
      			contentType : 'application/json',
      			data : JSON.stringify(cityInfo)
	    	})
	    	.success(function(res) {	    		
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };

	    this.selectedCity = function(cityId) {
	    	var defer = $q.defer();

	    	$http({
	    		url : api_base+'getCity?cityId='+cityId,
	    		method : 'GET',
	    		dataType : 'json',
	    		contentType : 'application/json'
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});

	    	return defer.promise;
	    };

	    this.categoryOffers = function (catId) {
    		
    		console.log("catId=== ",catId);
    		return $http.get(api_base + 'getCategory?catId=', {
		      params: {
		        query: catId
		      }
		    });
	    	
	    	
	    };

	    this.newBusiness = function (bizsData) {
    		var defer = $q.defer();
    		
    		$http({
    			url : api_base+'addBiz',
    			method : 'POST',
    			dataType : 'json',
      			contentType : 'application/json',
      			data : JSON.stringify(bizsData)
	    	})
	    	.success(function(res) {
	    		console.log('in service',res);
	    		defer.resolve('service resolve==>'+res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };

	    this.newOffer = function (offerData) {
	    	var defer = $q.defer();

	    	$http({
	    		url : api_base+'addOffer',
	    		method : 'POST',
	    		dataType : 'json',
	    		contentType : 'application/json',
	    		data : JSON.stringify(offerData)
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };

	    this.selectedOffer = function(offerId) {
	    	var defer = $q.defer();

	    	$http({
	    		url : api_base+'getOffer?offerId='+offerId,
	    		method : 'get',
	    		dataType : 'JSON',
	    		contentType : 'application/json'
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(res);
	    	});

	    	return defer.promise;
	    };

	    this.updatingOffer = function(offerData, offerId) {
	    	var defer = $q.defer();

	    	$http({
	    		url : api_base+'updateOffer?offerId='+offerId,
	    		method : 'POST',
	    		dataType : 'JSON',
	    		contentType : 'application/json',
	    		data : JSON.stringify(offerData)
	    	})
	    	.success(function(res) {
	    		console.log(res);
	    		defer.resolve(res);	    	
	    	})
	    	.error(function(err) {
	    		console.log(err);
	    		defer.reject(err);
	    	});
	    	return defer.promise;
	    };
  }]);
});