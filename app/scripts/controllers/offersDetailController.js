define(['modules/business', 'services/businessOffersService'] , function (offersDetailModule, businessService) {
	offersDetailModule.controller('offersDetailController', ['$rootScope','$location', '$scope' , '$routeParams','businessService', function ($rootScope, $location, $scope, $routeParams, businessService) {			
	  			// console.log($location.path().split('/')[2]);
			$scope.tog = 4;
  			var catId = $routeParams.catId;
  			console.log('Category Id ',catId);
  		

	  		businessService.selectedCategory(catId)
	  			.then(function (res) {
	  				console.log(res);
	  				$scope.currentCatName = res.catName;
	  				$scope.currentCatImage = res.catImage;
	  			},function(err) {
	  				console.log(err)
	  			});
	  		
	  	}
	  
	  ])
});