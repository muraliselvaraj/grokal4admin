define(['modules/business','services/businessOffersService'] , function (addBusinessModule, businessService) {
	addBusinessModule.controller('addBusinessController', ['$q', '$rootScope', '$scope' , '$routeParams','businessService', function ($q, $rootScope, $scope, $routeParams, businessService) {
		
			$scope.tog = 5;

			$scope.uploadLogo = function(files) {									        
				var file = document.querySelector('input[type=file]').files[0];
				var reader = new FileReader();
				var base64;
				var fileName;	
				reader.onloadend = function(e) {
					// base64 = reader.result;
					base64 = btoa(e.target.result);
					fileName = file.name;
					$scope.$apply(function () {
            			$scope.logo = {
            				name : fileName,
            				base64Data : base64
            			};
        			});
        			console.log('Logo==>',$scope.logo);        			
				}

				if (file) {
					// reader.readAsDataURL(file);
					reader.readAsBinaryString(file);
				} else {
					alert('Your Browser does not support File Reader..');
				}				 
			};
						
			$scope.images = [];
			$scope.uploadImages = function(files) {	
				console.log('changed');				
				var reader = new FileReader();
								
				
		        function readFile(index) {
		            if( index >= files.length ) return;
		            var file = files[index];
		            var multipleImgs;
					var multipleFileNames;
		            reader.onloadend = function(e) {  
		                // get file content		                 
		                multipleImgs = btoa(e.target.result);
		                multipleFileNames = file.name;
		                // do sth with multipleImgs
		                var imgObj = {
							name: multipleFileNames,
							base64Data: multipleImgs
						}
        				$scope.$apply(function () {	
        					$scope.images[index] = imgObj;
        				});
		                readFile(index+1);
		                console.log('Final==>',$scope.images);               		    
		            }
		            
		            reader.readAsBinaryString(file);	
		        }		        	        
		        readFile(0);
			};			

			var myLatLng = new google.maps.LatLng(12.971598700000000000, 77.594562699999980000);

	  		var mapOptions = {
		        zoom: 14,
		        center: myLatLng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    }

		    $scope.map = {};
    		$scope.map = new google.maps.Map(document.getElementById('mymap'), mapOptions);

    		
    		var marker = new google.maps.Marker({
    			position: myLatLng,
    			title:"Bangalore"
			});

			// To add the marker to the map, call setMap();
    		marker.setMap($scope.map);

    		$scope.LatLng = {};
    		// $scope.Lat = 0;

    		//Getting the LatLng by clicking on the map
    		google.maps.event.addListener($scope.map, "click", function (data) {
    			var latLng = data.latLng;
			    $scope.$apply(function () {
            		$scope.Lat = latLng.B;
			    	$scope.Lng = latLng.k;
        		});
        		placeMarker(latLng);
			});
			
			console.log($scope.LatLng);
			// Adding marker to clicked place
			function placeMarker(location) {				
				if (marker) {

					//if marker already was created change positon
					marker.setPosition(location);	
				} else {
					//create a marker
		        	marker = new google.maps.Marker({          
		            	position: location,
		            	map: $scope.map,
		            	draggable: true
		        	});
				}				
			}

			
			//getting all categories

			businessService.categoryList()
				.then(function(data){
					console.log(data.length);										
					$scope.catList = data;
				},function(err){
					console.log(err);
				});

			//getting all cites

			businessService.getAllCities()
				.then(function(data){
					console.log(data.length);										
					$scope.cityList = data;
				},function(err){
					console.log(err);
				});

			//add business click function
			
			$scope.addBusiness = function() {
				var bizsData = {
					bizs :[
						{
							bizName : $('#bizName').val(),
							description : $('#desc').val(),
							category : $('#cat').val(),
							phoneNumber : $('#phoneNumber').val(),
							line1 : $('#line1').val(),
							line2 : $('#line2').val(),
							city : $('#city').val(),
							lat : $scope.Lat,
							lng : $scope.Lng,
							logo : $scope.logo,
							images : $scope.images
						}
					]
				};

				console.log('New Business==>',JSON.stringify(bizsData));
				//calling service
				businessService.newBusiness(bizsData)
					//response
					.then(function (res) {
						alert(res);
					},function(error){
						alert('Error==>',error);
					});
			};
	  	}
	  
	  ])
});