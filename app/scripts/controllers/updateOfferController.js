define(['modules/business','services/businessOffersService'] , function (updateOfferModule, businessService) {
	updateOfferModule.controller('updateOfferController', ['$q', '$rootScope', '$scope' , '$routeParams','businessService', function ($q, $rootScope, $scope, $routeParams, businessService) {
		
			$scope.tog = 5;

			var offerId = $routeParams.offerId;

			//DateTimePicker Initialization
			$('#startDate').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm:ss', separator: 'T' });
			$('#endDate').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm:ss', separator: 'T' });
			$scope.parseStartDate = function() {
				$scope.startDate = $('#startDate').val();
				console.log('startDate==>',$scope.startDate);
			}
			
			$scope.parseEndDate = function() {
				$scope.endDate = $('#endDate').val();
				console.log('endDate==>',$scope.endDate);
			}

			businessService.selectedOffer(offerId)
				.then(function (res) {
					console.log(res);
					$scope.title = res.offers[0].title;
					$scope.description = res.offers[0].description;
					$scope.bizId = res.offers[0].bizId;
					$scope.radius = res.offers[0].radius;
					$scope.startDate = res.offers[0].startDate;
					$scope.endDate = res.offers[0].endDate;
				},function(err) {
					console.log(err);
				});
			//update offer click function
			
			$scope.updateOffer = function() {
				console.log('clicked');
				var offerData = {
					offers :[
						{
							title : $('#title').val(),
							description : $('#desc').val(),
							bizId : $scope.bizId,
							startDate : $('#startDate').val(),
							endDate : $('#endDate').val(),
							radius : $('#radius').val()
						}
					]
				};

				console.log('update offers==>',JSON.stringify(offerData));
				//calling service
				businessService.updatingOffer(offerData, offerId)
					//response
					.then(function (res) {
						console.log(res);
					},function(error){
						console.log('Error==>',error);
					});
			};
			
	  }])
});