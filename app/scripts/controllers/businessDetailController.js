define(['modules/business', 'services/businessOffersService'] , function (businessDetailModule, businessService) {
	businessDetailModule.controller('businessDetailController', ['$rootScope','$location', '$scope' , '$routeParams','businessService', function ($rootScope, $location, $scope, $routeParams, businessService) {			
	  			// console.log($location.path().split('/')[2]);
			$scope.tog = 5;
  			var bizId = $routeParams.bizId;
  			console.log('Business Id ',bizId);
  			businessService.businessOffers(bizId)
  				.then(function (data) {
	  				console.log('data ',data);
	  				$scope.businesses = data.offers;
	  				$scope.businessID = bizId;
  					
	  			},function(error) {
	  				console.log(error)
	  			});

	  		businessService.selectedBusiness(bizId)
	  			.then(function(res) {
	  				$scope.currentBusinessName = res.bizs[0].bizName;
	  				$scope.currentAddressLine1 = res.bizs[0].address.line1;
	  				$scope.currentAddressLine2 = res.bizs[0].address.line2;
	  				$scope.currentBusinessDescription = res.bizs[0].description;
	  				$scope.currentBusinessPhoneNumber = res.bizs[0].address.phoneNumber;
	  				$scope.currentBusinessLogo = res.bizs[0].logoUrl;
	  				$scope.currentBusinessViews = res.bizs[0].views;
	  				$scope.currentBusinessLikes = res.bizs[0].likes;
	  			},function(error) {
	  				console.log(error);
	  			});
	  		
	  	}
	  
	  ])
});