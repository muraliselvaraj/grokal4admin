define(['modules/business', 'services/businessOffersService'] , function (addCityModule, businessService) {
  	addCityModule.controller('addCityController', ['$q', '$rootScope', '$scope' , '$routeParams', 'businessService', function ($q, $rootScope, $scope, $routeParams, businessService) {
			$scope.tog = 3;
						
			businessService.getAllCities()
				.then(function(res) {
					$scope.cityId = res.length + 1;
				},function(err) {
					console.log(err);
				});
			//adding new category
			$scope.addCity = function() {
				var cityInfo = {
					cityId : $scope.cityId,
					cityName : $scope.cityName
				};

				console.log('New Business==>',cityInfo);
				//calling service
				businessService.newCity(cityInfo)
					//response
					.then(function (res) {						
						console.log(res);
						alert(res);
					},function(error){
						alert('Error==>',error);
					});
			};
	  	}
	  
	  ])
});