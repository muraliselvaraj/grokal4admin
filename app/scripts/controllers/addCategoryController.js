define(['modules/business', 'services/businessOffersService'] , function (addCategoryModule, businessService) {
  	addCategoryModule.controller('addCategoryController', ['$q', '$rootScope', '$scope' , '$routeParams', 'businessService', function ($q, $rootScope, $scope, $routeParams, businessService) {
			$scope.tog = 4;
			// category logo uplad
			$scope.uploadLogo = function(files) {									        
				var file = document.querySelector('input[type=file]').files[0];
				var reader = new FileReader();
				var base64;
				var fName;	
				reader.onloadend = function(e) {
					// base64 = reader.result;
					base64 = btoa(e.target.result);
					fileName = file.name;
					$scope.$apply(function () {
            			$scope.catImage = {
            				name : fileName,
            				base64Data : base64
            			};
        			});
        			console.log('Logo==>',$scope.catImage);        			
				}

				if (file) {
					// reader.readAsDataURL(file);
					reader.readAsBinaryString(file);
				} else {
					alert('Your Browser does not support File Reader..');
				}				 
			};

			businessService.categoryList()
				.then(function(res) {
					$scope.catId = res.length + 1;
				},function(err) {
					console.log(err);
				});
			//adding new category
			$scope.addCategory = function() {
				var categoryInfo = {
					catId : $scope.catId,
					catName : $scope.catName,
					catImage : $scope.catImage
				};

				console.log('New Business==>',categoryInfo);
				//calling service
				businessService.newCategory(categoryInfo)
					//response
					.then(function (res) {
						alert('Added==>'+res);
					},function(error){
						alert('Error==>',error);
					});
			};
	  	}
	  
	  ])
});