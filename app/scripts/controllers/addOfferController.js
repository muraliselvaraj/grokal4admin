define(['modules/business','services/businessOffersService'] , function (addOfferModule, businessService) {
	addOfferModule.controller('addOfferController', ['$q', '$rootScope', '$scope' , '$routeParams','businessService', function ($q, $rootScope, $scope, $routeParams, businessService) {
		
			$scope.tog = 5;

			//DateTimePicker Initialization
			$('#startDate').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm:ss', separator: 'T' });
			$('#endDate').datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'HH:mm:ss', separator: 'T' });
			$scope.parseStartDate = function() {
				
				$scope.startDate = $('#startDate').val();
				console.log('startDate==>',$scope.startDate);
			}
			
			$scope.parseEndDate = function() {
				$scope.endDate = $('#endDate').val();
				console.log('endDate==>',$scope.endDate);
			}

			$scope.bizId = $routeParams.businessID;
			$scope.uploadImage = function(files) {									        
				var file = document.querySelector('input[type=file]').files[0];
				var reader = new FileReader();
				var base64;
				var fName;	
				reader.onloadend = function(e) {					
					base64 = btoa(e.target.result);
					fileName = file.name;
					$scope.$apply(function () {
						// $scope.image = base64;
            			$scope.image = {
            				name : fileName,
            				base64Data : base64
            			};
        			});        			
        			console.log('Image Data==>',$scope.image);        			
				}
				if (file) {
					reader.readAsBinaryString(file);
				} else {
					alert('Your Browser does not support File Reader..');
				}				 
			};

			//add offer click function
			
			$scope.addOffer = function() {
				console.log('clicked');
				var offerData = {
					offers :[
						{
							title : $('#title').val(),
							description : $('#desc').val(),
							bizId : $scope.bizId,
							startDate : $('#startDate').val(),
							endDate : $('#endDate').val(),
							radius : $('#radius').val(),
							image : $scope.image
						}
					]
				};

				console.log('New offers==>',JSON.stringify(offerData));
				//calling service
				businessService.newOffer(offerData)
					//response
					.then(function (res) {
						alert(res);
					},function(error){
						alert('Error==>',error);
					});
			};
			
	  }])
});